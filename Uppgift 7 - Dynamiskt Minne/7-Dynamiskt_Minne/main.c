#include <stdio.h>
#include "groceryList.h"
#include "tryInput.h"
#include "memorymanagement.h"
#include "filemanagement.h"

int main(){
    _Bool running = 1;
    shoppingList groceries;

    groceries.itemsInList = 0;
    groceries.ShoppingListItem = NULL;

    while(running){
        int choice = tryInput("1. Add item\n2. Print list\n3. Remove item\n"
                              "4. Edit item\n5. Save list\n6. Load list\n7. Quit\nChoice: ", "\nPlease choose one.\n\n");

        switch(choice){
        case 1:
            AddItemToList(&groceries);
            break;
        case 2:
            printList(&groceries);
            break;
        case 3:
            removeItemFromList(&groceries);
            break;
        case 4:
            editItemInList(&groceries);
            break;
        case 5:
            saveListToFile(&groceries);
            break;
        case 6:
            loadListFromFile(&groceries);
            break;
        case 7:
            running = 0;
            free(groceries.ShoppingListItem);
            break;
        default:
            printf("\nPlease choose one.\n\n");
        }
    }

    free(groceries.ShoppingListItem);
    return 0;
}
