#ifndef _TRYINPUT_H_
#define _TRYINPUT_H_

#include <stdio.h>

/*Checks for a number input, if the correct input is given, and if
  no waste (anythin but '\n') is entered afterwards*/
int checkInputForErrors(float input, char catch, int scanfOutput);
//Tries to take new input from user every time it fails
float tryInput(char output[], char failedOutput[]);
//Gets input, replaces dots with \0, then adds extension .txt to end of string
void inputFileName(char input[]);

#endif
