#ifndef _MEMORYMANAGEMENT_H_
#define _MEMORYMANAGEMENT_H_

#include <stdlib.h>
#include "groceryList.h"

//Tries to allocate memory with calloc, then returns grocerItem pointer, performs errorchecking
groceryItem* tryToAllocatieMemory(unsigned long int sizeOfNewMemBlock);
//Tries to reallocate memory of groceryItem pointer, performs errorchecking
void tryToReallocateMemory(groceryItem **oldMemBlock, unsigned long int sizeOfNewMemBlock);

#endif
