#include "groceryList.h"
#include "tryInput.h"
#include "memorymanagement.h"

void AddItemToList(shoppingList *pGroceries){
    if(pGroceries->itemsInList == 0){
        pGroceries->ShoppingListItem = tryToAllocatieMemory(1);
    }else{
        tryToReallocateMemory(&pGroceries->ShoppingListItem, pGroceries->itemsInList + 1);
    }

    system("clear");
    printf("Item: ");
    gets(pGroceries->ShoppingListItem[pGroceries->itemsInList].item);
    pGroceries->ShoppingListItem[pGroceries->itemsInList].item[ITEMSIZE - 1] = '\0';

    pGroceries->ShoppingListItem[pGroceries->itemsInList].amount = tryInput("Amount: ", "Invalid input. Please try again.\n");

    printf("Unit: ");
    gets(pGroceries->ShoppingListItem[pGroceries->itemsInList].unit);
    pGroceries->ShoppingListItem[pGroceries->itemsInList].unit[UNITSIZE - 1] = '\0';
    printf("\n");
    system("clear");

    pGroceries->itemsInList++;
}

void printList(shoppingList *pGroceries){
    system("clear");
    if(pGroceries->itemsInList == 0){
        printf("Your shopping list is empty.\n");
    }else{
        printf("+--+--------------------------------------------------+\n");
        printf("|# | Item                            Amount Unit      |\n");
        printf("+--+--------------------------------------------------+\n");
        for(int i = 0; i < pGroceries->itemsInList; i++){
            printf("|%-2d| %-20s\t", i + 1, pGroceries->ShoppingListItem[i].item);
            printf("%11.6g ", pGroceries->ShoppingListItem[i].amount);
            printf("%-10s|\n", pGroceries->ShoppingListItem[i].unit);
        }
        printf("+--+--------------------------------------------------+\n");
    }
    printf("\n");
}

void removeItemFromList(shoppingList *pGroceries){
    if(pGroceries->itemsInList == 0){
        printf("\nYou have no items to remove.\n");
    }else{
        int choice = tryInput("\nItem to remove: ", "Invalid input, please try again.");

        if(choice > pGroceries->itemsInList || choice < 1){
            printf("There is no item on position %d.\n", choice);
        }else{
            for(int i = choice - 1; i < pGroceries->itemsInList; i++){
                pGroceries->ShoppingListItem[i] = pGroceries->ShoppingListItem[i+1];
            }
            pGroceries->itemsInList--;
            tryToReallocateMemory(&pGroceries->ShoppingListItem, pGroceries->itemsInList);
        }
    }
    printf("\n");
}

void editItemInList(shoppingList *pGroceries){
    if(pGroceries->itemsInList == 0){
        printf("You have no items to edit.\n");
    }else{
        int choice = tryInput("\nItem to edit: ", "Invalid input, please try again.");

        if(choice > pGroceries->itemsInList || choice < 1){
            printf("There is no item on position %d.\n", choice);
        }else{
            float tmpInput = tryInput("New amount: ", "Invalid input. Please try again.\n");
            pGroceries->ShoppingListItem[choice - 1].amount = tmpInput;
        }
    }
    printf("\n");
}
