#include "tryInput.h"

int checkInputForErrors(float input, char catch, int scanfOutput){
    if(input < 0 || scanfOutput == 0 || catch != '\n'){
        if(scanfOutput == 0){
            while(getchar() != '\n');
        }
        return 1;
    }

    return 0;
}

float tryInput(char output[], char failedOutput[]){
    int scanfOutput, errorCheck;
    float choice;
    char catchNewLine;

    do{
        printf(output);
        scanfOutput = scanf("%f%c", &choice, &catchNewLine);
        errorCheck = checkInputForErrors(choice, catchNewLine, scanfOutput);
        if(errorCheck != 0){
            printf(failedOutput);
        }
    }while(errorCheck != 0);

    return choice;
}

void inputFileName(char input[]){
    gets(input);

    for(int i = 0; input[i] != '\0'; i++){
        if(input[i] == '.'){
            input[i] = '\0';
        }
    }

    sprintf(input, "%s.txt", input);
}
