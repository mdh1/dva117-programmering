TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.c \
    groceryList.c \
    tryInput.c \
    memoryManagement.c \
    fileManagement.c

HEADERS += \
    groceryList.h \
    tryInput.h \
    memorymanagement.h \
    filemanagement.h
